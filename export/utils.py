import os
import pandas as pd

def get_snaphot(stream,snapshot_dir):
    if os.path.isfile(f"{snapshot_dir}/{stream}.snapshot.csv"):
        snapshot = pd.read_csv(f"{snapshot_dir}/{stream}.snapshot.csv")
    else:
        snapshot = None

    return snapshot

def snapshot_records(stream_data, stream, snapshot_dir, pk="id", return_updated = False):
    if stream_data is None:
        return None

    return_data = None

    if os.path.isfile(f"{snapshot_dir}/{stream}.snapshot.csv"):
        snapshot = pd.read_csv(f"{snapshot_dir}/{stream}.snapshot.csv")
    else:
        snapshot = None

    if snapshot is not None:
        # If snapshot is present, drop any duplicats from the return data
        stream_data = pd.concat([snapshot, stream_data])
        return_data = stream_data.drop_duplicates(pk, keep=False)
        # Save the updated snapshot
        stream_data = stream_data.drop_duplicates(pk, keep="last")
        stream_data.to_csv(f"{snapshot_dir}/{stream}.snapshot.csv", index=False)
    else:
        # Otherwise, snapshot and use stream_data as the return
        stream_data.to_csv(f"{snapshot_dir}/{stream}.snapshot.csv", index=False)
        return_data = stream_data

    return return_data


def update_and_snapshot(stream_data, stream, snapshot_dir, pk="id",just_new = False) -> list:
    """
    Snapshotting logic suitable for incremental sync in the case that we DO NOT want the 
    old data to be sent toguether along with the new data. 

    stream_data : pd.DataFrame containing the data from the sync.
    stream : stream name to be written on the snapshot csv file 
    snapshot_dir : snapshot folder 
    pk : primary key to check for duplicates and updated

    """
    if stream_data is None:
        if just_new:
            return None
        return None,None

    # Return data [new_records,updated_records]
    updated_records = None 
    new_records = None 


    if os.path.isfile(f"{snapshot_dir}/{stream}.snapshot.csv"):
        snapshot = pd.read_csv(f"{snapshot_dir}/{stream}.snapshot.csv")
    else:
        snapshot = None

    if snapshot is not None:
        # If snapshot is present:
        
        # The stream data that is present in the snapshot 
        mask = stream_data[pk].isin(list(snapshot[pk]))
        
        # New records are not in the snapshot 
        new_records = stream_data[~mask]
        if not just_new: 
            # Updated records are in the snapshot but appeared again 
            updated_records = stream_data[mask]


        # Snapshot the data
        stream_data = pd.concat([snapshot, stream_data])
        stream_data = stream_data.drop_duplicates(pk, keep="last")
        stream_data.to_csv(f"{snapshot_dir}/{stream}.snapshot.csv", index=False)
    else:
        # Otherwise, snapshot and use stream_data as the return
        stream_data.to_csv(f"{snapshot_dir}/{stream}.snapshot.csv", index=False)
        new_records = stream_data

    # return just new_records
    if just_new:
        return new_records
    
    return_data = new_records,updated_records
    return return_data